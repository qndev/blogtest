---
layout: page
title: About me
excerpt: "About me."
modified:
---

### về tôi...
<img align="left" width="160" height="160" src="/blog/images/aboutme.jpeg" style="margin-right: 15px;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);border-radius: 3px;"> Xin chào, tôi là <strong>Nguyễn Đình Quang</strong> và trang web nhỏ này là ngôi nhà internet của tôi.

Cám ơn các bạn đã ghé thăm blog!
Nếu có câu hỏi, Bạn có thể để lại comment bên dưới hoặc gửi email về hòm thư quangnd.hust@gmail.com.

[Résumé/CV]() </> [GitHub](https://github.com/qnDev) </> [Facebook](https://www.facebook.com/quangnd.hust)

### về blog...
Một nơi tôi có thể chia sẻ một số suy nghĩ, kinh nghiệm, ý kiến, đánh giá, ý tưởng với mục đích nâng cao khả năng diễn đạt, trình bày vấn đề. Nhưng hầu hết thời gian, tôi chia sẻ về các dự án cá nhân, nghiên cứu và lập trình của tôi.

### tôi đã xây dựng blog như thế nào...
Blog này được xây dựng dựa trên Jekyll, một trình tạo trang web tĩnh, đơn giản cho các trang web cá nhân. Nó được phân phối theo giấy phép MIT nguồn mở. Template tôi dùng cho blog này là [So Simple Theme](https://mademistakes.com/work/so-simple-jekyll-theme/).
